# soal-shift-sisop-modul-4-A09-2022

Laporan Resmi Soal Shift Modul 4 Kelompok A09 Sistem Operasi 2022

Anggota kelompok:

1. Hafizh Mufid Darussalam 5025201093

2. Moh Akmal Ali Dzikri 5025201204

3. Rivaldo Panangian Tambunan 5025201134

# Penyelesaian
Note: filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root)
filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’
diabaikan, dan ekstensi tidak perlu di-encode

## Nomor 1
### a. Pada direktori dengan awalan "Animeku_" akan dilakukan encode terhadap isi direktori tersebut. Huruf besar akan ter-encode dengan atbash cipher dan huruf kecil akan ter-encode dengan rot13. 
Pertama, perlu meng-include library yang akan diperlukan dan inisialisasi variabel:
```
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/time.h>
#define maxSize 1024
char dirpath[maxSize];
```
Kemudian, inisialisasikan fungsi yang akan digunakan untuk encode. Ketentuan encodenya adalah sebagai berikut:

Encode atbash cipher untuk huruf besar:

A -> Z, B -> Y, C -> X, dan seterusnya. Berlaku pula untuk sebaliknya, misalnya Z akan di-encode menjadi A.

Encode rot13 untuk huruf kecil:

a -> n, b -> o, c -> p, dan seterusnya. Berlaku pula untuk sebaliknya, misalnya n akan di-encode menjadi a.

```
//melakukan encode/decode
char * Clem_code(char src[]) {
  char str[maxSize];
  strcpy(str, src);
  int i;
  for (i = 0; i < strlen(src); i++) {
  	if (src[i] >= 65 && src[i] <= 90) {
		str[i] = 'Z'+'A'- src[i];		
	}
  	if (src[i] >= 97 && src[i] <= 122) {
  		if (src[i] >= 97 && src[i] <= 109) {
  			str[i] = src[i] + 13;
  		}
  		if (src[i] >= 110 && src[i] <= 122) {
  			str[i] = src[i] - 13;
  		}		
	}

  }
  char * res = str;
  return res;
}
```

Huruf besar dalam ASCII berada dalam rentang nomor 65 (A) hingga 90 (Z), sedangkan huruf kecil berada dalam rentang 97 (a) hingga 122 (z). Alasan huruf kecil dibagi dua, yakni a hingga m dan n hingga z adalah untuk keperluan encode di mana pada encode jenis rot13, 26 huruf akan dibagi dua menjadi 13 huruf masing-masingnya. Dua bagian tersebut terdiri dari a-m dan n-z. Kemudian, kita akan membuat fungsi untuk mendapatkan path.

```
char * find_path(const char * path) {
  char fpath[2 * maxSize];
  bool flag, toMirror = 0;
  //bool toMirror2 = 0;

  char * strMir;
  //char * strmir2;
  if (strcmp(path, "/"))
  {
    // /home/[USER]/Documents/Clem_abc/afgh.jpg
	strMir = strstr(path, "/Animeku_");
	if (strMir) {
  	toMirror = 1;
  	strMir++;
	}
	/*strMir2 = strstr(path, "/IAN_");
	if (strMir2) {
  	toMirror2 = 1;
  	strMir2++;
	}*/	
  }

  if (strcmp(path, "/") == 0) {
	sprintf(fpath, "%s", dirpath);
  } else if (toMirror) {
	char pathOrigin[maxSize] = "";
	char t[maxSize];

	strncpy(pathOrigin, path, strlen(path) - strlen(strMir));
	strcpy(t, strMir);

	char * selectedFile;
	char * rest = t;

	flag = 0;
    // /home/[USER]/Documents/Clem_abc/afgh.jpg
	while ((selectedFile = strtok_r(rest, "/", & rest))) {
        if (!flag) {
            strcat(pathOrigin, selectedFile);
            flag = 1;
            continue;
        }

        char checkType[2 * maxSize];
        sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
        strcat(pathOrigin, "/");

        if (strlen(checkType) == strlen(path)) {
            char pathFolder[2 * maxSize];
            sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

            DIR * dp = opendir(pathFolder);
            if (!dp) { // Cek apakah ini file/bukan
                char * ext;
                ext = strrchr(selectedFile, '.'); // abc.tar.gz

                char fileName[maxSize] = "";
                if (ext)
                {
                    strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
                    sprintf(fileName, "%s%s", Clem_code(fileName), ext);
                } else if (toMirror)
                    strcpy(fileName, Clem_code(selectedFile));

                printf("%s\n", fileName);
                strcat(pathOrigin, fileName);
            } else
                strcat(pathOrigin, Clem_code(selectedFile));

        } else {
            strcat(pathOrigin, Clem_code(selectedFile));
        }
	}
	sprintf(fpath, "%s%s", dirpath, pathOrigin);
  } /*else if (toMirror) {
	char pathOrigin[maxSize] = "";
	char t[maxSize];

	strncpy(pathOrigin, path, strlen(path) - strlen(strMir));
	strcpy(t, strMir);

	char * selectedFile;
	char * rest = t;

	flag = 0;
    // /home/[USER]/Documents/Clem_abc/afgh.jpg
	while ((selectedFile = strtok_r(rest, "/", & rest))) {
        if (!flag) {
            strcat(pathOrigin, selectedFile);
            flag = 1;
            continue;
        }

        char checkType[2 * maxSize];
        sprintf(checkType, "%s/%s", pathOrigin, selectedFile);
        strcat(pathOrigin, "/");

        if (strlen(checkType) == strlen(path)) {
            char pathFolder[2 * maxSize];
            sprintf(pathFolder, "%s%s%s", dirpath, pathOrigin, selectedFile);

            DIR * dp = opendir(pathFolder);
            if (!dp) { // Cek apakah ini file/bukan
                char * ext;
                ext = strrchr(selectedFile, '.'); // abc.tar.gz

                char fileName[maxSize] = "";
                if (ext)
                {
                    strncpy(fileName, selectedFile, strlen(selectedFile) - strlen(ext));
                    sprintf(fileName, "%s%s", Clem_code(fileName), ext);
                } else if (toMirror)
                    strcpy(fileName, Clem_code(selectedFile));

                printf("%s\n", fileName);
                strcat(pathOrigin, fileName);
            } else
                strcat(pathOrigin, Clem_code(selectedFile));

        } else {
            strcat(pathOrigin, Clem_code(selectedFile));
        }
	}
	sprintf(fpath, "%s%s", dirpath, pathOrigin);
  }*/
  else
	sprintf(fpath, "%s%s", dirpath, path);

  char * fpath_to_return = fpath;
  return fpath_to_return;
}
```

Setelah itu, inisialisasikan fungsi yang termasuk ke dalam fuse operations. Dalam hal ini, digunakan tiga jenis fungsi yakni xmp_readdir, xmp_read, dan xmp_getattr. Secara khusus, fungsi xmp_readdir akan digunakan untuk membantu jalannya kegiatan encode dan di dalamnya akan memanggil fungsi find_path dan Clem_code.

```
static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {
  char fpath[maxSize];
  bool toMirror = strstr(path, "/Animeku_");
  //bool toMirror2 = strstr(path, "/IAN_");
  strcpy(fpath, find_path(path));

  int res = 0;
  DIR * dp;
  struct dirent * de;

  (void) offset;
  (void) fi;

  dp = opendir(fpath);
  if (dp == NULL)
	return -errno;

  while ((de = readdir(dp)) != NULL) {
	struct stat st;
	memset( & st, 0, sizeof(st));
	st.st_ino = de -> d_ino;
	st.st_mode = de -> d_type << 12;

	if (strcmp(de -> d_name, ".") == 0 || strcmp(de -> d_name, "..") == 0) {
  	    res = (filler(buf, de -> d_name, & st, 0));
	} else if (toMirror) {
        if (de -> d_type & DT_DIR) { // Apakah dia folder
            char temp[maxSize];
            strcpy(temp, de -> d_name);

            res = (filler(buf, Clem_code(temp), & st, 0));
        } else { // Berupa file
            char * ext;
            ext = strrchr(de -> d_name, '.');

            char fileName[maxSize] = "";
            if (ext) {
                // abc.jpg
                strncpy(fileName, de -> d_name, strlen(de -> d_name) - strlen(ext)); // -> abc
                strcpy(fileName, Clem_code(fileName)); // -> cba (hasil encode)
                strcat(fileName, ext); // -> cba.jpg (hasil akhir encode)
            } else {
                strcpy(fileName, Clem_code(de -> d_name));
            }
            res = (filler(buf, fileName, & st, 0));
        }
	} else
  	    res = (filler(buf, de -> d_name, & st, 0));

	if (res != 0)
  	    break;
  }
  closedir(dp);
  return 0;
}

static int xmp_read(const char * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
  char fpath[maxSize];
  strcpy(fpath, find_path(path));

  int fd;
  int res;

  (void) fi;
  fd = open(fpath, O_RDONLY);
  if (fd == -1)
	return -ENONET;

  res = pread(fd, buf, size, offset);
  if (res == -1)
	res = -ENONET;

  close(fd);
  return res;
}

static int xmp_getattr(const char * path, struct stat * stbuf) {
  char fpath[maxSize];
  strcpy(fpath, find_path(path));

  int res;
  res = lstat(fpath, stbuf);
  if (res == -1)
	return -ENONET;

  return 0;
}
```

Setelah itu, dibuat struct untuk menampung fungsi-fungsi fuse.

```
static struct fuse_operations xmp_oper = {
  .getattr = xmp_getattr,
  .readdir = xmp_readdir,
  .read = xmp_read,
};

```

Terakhir, fungsi utama.

```
int main(int argc, char * argv[]) {
  //char * username = getenv("USER");
  char * username = "/home/hapis/Documents"; // Opsional, deklarasi path secara manual

  sprintf(dirpath, "%s", username);

  umask(0);

  return fuse_main(argc, argv, & xmp_oper, NULL);
}
```
Di bawah ini adalah gambar dari proses menjalankan program fuse:

![Run program](image/Frame_24.png)

Di bawah ini adalah gambar dari proses encode isi direktori yang memiliki nama berawalan dengan "Animeku_":

![Encode](image/Frame_23.png)

### b. Ketika sebuah direktori di-rename menjadi berawalan "Animeku_", isi direktori tersebut akan otomatis ter-encode.
![Rename encode](image/Frame_25.png)

### c. Ketika sebuah direktori berawalan "Animeku_" di-rename menjadi tidak berawalan "Animeku_", isi direktori tersebut akan otomatis ter-decode.
![Rename decode](image/Frame_26.png)

### d. Data mengenai kegiatan encode dan decode akan dimasukkan ke dalam sebuah log.

### e. Metode encode akan bekerja secara rekursif.
Proses encode akan berjalan dengan rekursif. Jadi jika ada sebuah direktori bernama berawalan "Animeku_", isi direktori di dalamnya dan isi direktori di dalamnya lagi akan terencode.

![Rekursif encode](image/Frame_27.png)

## Nomor 2
Nomor 2 tidak banyak berbeda dengan nomor 1, hanya saja untuk encode menggunakan Vigenere Cipher, tidak seperti nomor 1 yang menggunakan atbash cipher dan rot13.

## Nomor 3

## Dokumentasi Pengerjaan dan Kendala
Dokumentasi:
![Dokum](image/Frame_28.png)

Kendala:

- Kurang paham konsep fuse dan implementasinya

![Dokum](image/Frame_29.png)
